#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "imagem.hpp"
#include "filtros.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include "matriz.hpp"
#include "pretoebranco.hpp"

int main(int argc, char ** argv) {

int opcao;

cout << "\n=======================================================" << endl;
cout << "Bem-vindo ao Aplicador de filtros!" << endl;
cout << "=======================================================" << endl;
cout << "===================" << endl;
cout << "|1-Polarizado     |" << endl;
cout << "|2-Negativo       |" <<endl;
cout << "|3-Preto e branco |" << endl;
cout << "|4-Media matriz   |" << endl;
cout << "===================\n" << endl;
cout << "Digite a opção equivalente ao filtro que deseja aplicar.\n" << endl;
cin >> opcao;

try {
	if (opcao == 1) {
	Polarizado * imagem = new Polarizado();
	imagem->AbrirImagem();
	imagem->ExtrairAtributos();
	imagem->ExtrairHead();

	if (imagem->getNumeroMagico() != "P6") {
	cout << "Tipo de imagem inválido!" << endl;
	exit(1);
	} else {
	imagem->AplicaFiltro();

	cout << "Tipo de imagem: " << imagem->getNumeroMagico() << endl;
	cout << "Largura: " << imagem->getLargura() << endl;
	cout << "Altura: " << imagem->getAltura() << endl;

	}
	} else

	if (opcao == 2) {
        Negativo * imagem = new Negativo();
        imagem->AbrirImagem();
        imagem->ExtrairAtributos();
        imagem->ExtrairHead();

        if (imagem->getNumeroMagico() != "P6") {
        cout << "Tipo de imagem inválido!" << endl;
        exit(1);
        } else {
        imagem->AplicaFiltro();

        cout << "Tipo de imagem: " << imagem->getNumeroMagico() << endl;
        cout << "Largura: " << imagem->getLargura() << endl;
        cout << "Altura: " << imagem->getAltura() << endl;

        }
        } else

	if (opcao == 3) {
        Pretoebranco * imagem = new Pretoebranco();
        imagem->AbrirImagem();
        imagem->ExtrairAtributos();
        imagem->ExtrairHead();

        if (imagem->getNumeroMagico() != "P6") {
        cout << "Tipo de imagem inválido!" << endl;
        exit(1);
        } else {
        imagem->AplicaFiltro();

        cout << "Tipo de imagem: " << imagem->getNumeroMagico() << endl;
        cout << "Largura: " << imagem->getLargura() << endl;
        cout << "Altura: " << imagem->getAltura() << endl;

        }
        } else

	if (opcao == 4) {
        Matriz * imagem = new Matriz();
        imagem->AbrirImagem();
        imagem->ExtrairAtributos();
        imagem->ExtrairHead();

        if (imagem->getNumeroMagico() != "P6") {
        cout << "Tipo de imagem inválido!" << endl;
        exit(1);
        } else {
        imagem->AplicaFiltro();

        cout << "Tipo de imagem: " << imagem->getNumeroMagico() << endl;
        cout << "Largura: " << imagem->getLargura() << endl;
        cout << "Altura: " << imagem->getAltura() << endl;

        }
        } else

	if ((opcao != 1) || (opcao != 2) || (opcao != 3) || (opcao != 4)) {
		throw(6);
	}
}
catch (int le_teclado) {
	cout << "Opção invalida!" << endl;

}
return 0;
}
