#include "pretoebranco.hpp"
#include <stdlib.h>
#include <fstream>

using namespace std;

Pretoebranco::Pretoebranco() {
	setNumeroMagico("");
	setImagem("");
	setHeadSize(0);
	setLargura(0);
	setAltura(0);
	setTamanhoImagem(0);
	setTamanhoTotal(0);
}

Pretoebranco::~Pretoebranco() {

}

void Pretoebranco::AplicaFiltro() {
	int i;
	int contador = 0;
	string nome = "";
	string r, g, b;
	char imagem_filtro;

	cout << "Insira um novo nome para a imagem com filtro preto e branco: " << endl;
	cin >> nome;
	ofstream nova_imagem;
	nova_imagem.open(nome.c_str());


	for (i = 0; i < getHeadSize(); i++) {
	nova_imagem << getImagem()[i];
	}
	nova_imagem << '\n';
		for (int j = i+1; j < getTamanhoTotal(); j++) {
 		contador++;
 		if (contador == 1) {
 		r += getImagem()[j];
		} else
		if (contador == 2) {
 		g += getImagem()[j];
		} else {
		b += getImagem()[j];
 		contador=0;
 		}
	}
	for (i = 0; i < getTamanhoImagem(); i++) {
 		imagem_filtro = (unsigned char) (0.299 * r[i]) + (0.587 * g[i]) + (0.144 * b[i]);
 		if (imagem_filtro > getNivelCor()){
		imagem_filtro = getNivelCor();
        }
 		nova_imagem << (unsigned char) imagem_filtro;
 		nova_imagem << (unsigned char) imagem_filtro;
 		nova_imagem << (unsigned char) imagem_filtro;
 	}

nova_imagem.close();

}
