#include "imagem.hpp"
#include <stdio.h>
#include <fstream>
#include <stdlib.h>

//construtor
ImagemPPM::ImagemPPM() {
	imagem = "";
	numero_magico = "";
	nivel_cor = 0;
	tamanho_total = 0;
	tamanho_imagem = 0;
	largura = 0;
	altura = 0;
	head_size = 0;

}
//destrutor
ImagemPPM::~ImagemPPM() {
}
//métodos acessores
string ImagemPPM::getImagem() {
	return imagem;
}
void ImagemPPM::setImagem(string imagem) {
	this-> imagem = imagem;
}
string ImagemPPM::getNumeroMagico() {
        return numero_magico;
}
void ImagemPPM::setNumeroMagico(string numero_magico) {
        this-> numero_magico = numero_magico;
}
unsigned char ImagemPPM::getNivelCor() {
        return nivel_cor;
}
void ImagemPPM::setNivelCor(unsigned char nivel_cor) {
        this-> nivel_cor = nivel_cor;
}
int ImagemPPM::getTamanhoTotal() {
        return tamanho_total;
}
void ImagemPPM::setTamanhoTotal(int tamanho_total) {
        this-> tamanho_total = tamanho_total;
}
int ImagemPPM::getTamanhoImagem() {
        return tamanho_imagem;
}
void ImagemPPM::setTamanhoImagem(int tamanho_imagem) {
        this-> tamanho_imagem = tamanho_imagem;
}
int ImagemPPM::getLargura() {
        return largura;
}
void ImagemPPM::setLargura(int largura) {
        this-> largura = largura;
}
int ImagemPPM::getAltura() {
        return altura;
}
void ImagemPPM::setAltura(int altura) {
        this-> altura = altura;
}
int ImagemPPM::getHeadSize() {
	return head_size;
}
void ImagemPPM::setHeadSize(int head_size) {
	this-> head_size = head_size;
}
//função para abrir a imagem e salvar as informações
void ImagemPPM::AbrirImagem() {
	string imagem = "";
	string nomeimg = "";
	int contador = 0;

	cout << "\nInsira o nome da imagem que deseja modificar,\ncertifique que a mesma se encontra na pasta raiz do aplicativo:\n " << endl;
	cin >> nomeimg;

	ifstream inFile;
	inFile.open(nomeimg.c_str());

	if (inFile.fail()) {
	cerr << "Nome invalido ou arquivo inexistente." << endl;
	exit(1);
	}

	while (!inFile.eof()) {
		imagem+= inFile.get();
		contador++;
	}

	this-> imagem = imagem;
	tamanho_total = contador;

}
//função para extrair os dados do cabeçalho
void ImagemPPM::ExtrairAtributos() {
	string numero_magico;
	string largura;
	string altura;
	string nivel_cor;
	string comentario;
	int contador = 0;
	int tamanho_imagem = 0;

	for (int i = 0; i < tamanho_total; i++) {

	if (imagem[i] == '\n') {
			contador++;
		}
		if ((imagem[i] != '\n') && (contador == 0)) {
			numero_magico += imagem[i];
		} else

		if (imagem [i] == '#') {
			comentario += imagem[i];
			contador = 1;
		} else

		 if ((contador == 2) && (imagem[i] != ' ')) {
			largura += imagem[i];
		} else

		if ((contador == 2) && (imagem[i] == ' ' )) {
			contador++;
		} else

		if (contador == 3) {
			altura += imagem[i];
		} else

		if (imagem[i] == 4) {
			nivel_cor += imagem[i];
		} else

		if (imagem[i] == 5) {
			break;
		}


	}
//definindo a conversao de strings em inteiros
	tamanho_imagem = atoi(largura.c_str())*atoi(altura.c_str());
	this-> numero_magico = numero_magico;
	this-> largura = atoi(largura.c_str());
        this-> altura = atoi(altura.c_str());
	this-> tamanho_imagem = tamanho_imagem;
        this-> nivel_cor = atoi(nivel_cor.c_str());
}
void ImagemPPM::ExtrairHead() {
	int head = 0;
	int contador = 0;
	string comentario;

	for(int i=0; i < tamanho_total; i++) {
		if (imagem[i] == '\n') {
		contador++;
		}
		if (imagem[i] == '#') {
		comentario += imagem[i];
		contador = 1;
		} else
		if (contador == 4) {
		break;
		}
		head++;
	}
	head_size = head;

}
