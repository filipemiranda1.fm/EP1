#include <fstream>
#include <stdlib.h>
#include "negativo.hpp"

using namespace std;
//construtor
Negativo::Negativo() {
	setImagem("");
	setHeadSize(0);
	setLargura(0);
	setAltura(0);
	setTamanhoTotal(0);
	setNumeroMagico("");
	setTamanhoImagem(0);
}
//destrutor
Negativo::~Negativo() {

}
//função do filtro negativo
void Negativo::AplicaFiltro() {
	string nome = "";
	int i;

	cout <<"Insira o novo nome da imagem com filtro negativo: " << endl;
	cin >> nome;

	ofstream nova_imagem;
	nova_imagem.open(nome.c_str());

	for (i=0; i < getHeadSize(); i++) {
	nova_imagem << getImagem()[i];
	}
	nova_imagem << '\n';
	for (i=i+1; i < getTamanhoTotal(); i++) {
	nova_imagem << (unsigned char) (getNivelCor()-getImagem()[i]);
	}
nova_imagem.close();
}
