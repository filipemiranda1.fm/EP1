#include <fstream>
#include <stdlib.h>
#include "polarizado.hpp"

using namespace std;

Polarizado::Polarizado() {
	setImagem("");
	setHeadSize(0);
	setNumeroMagico("");
	setTamanhoImagem(0);
	setTamanhoTotal(0);
	setAltura(0);
	setLargura(0);
}

Polarizado::~Polarizado() {

}

void Polarizado::AplicaFiltro() {
	string nome = "";
	int i;
	cout << "Insira o nome que deseja salvar a nova imagem: " << endl;
	cin >> nome;
	ofstream nova_imagem;
	nova_imagem.open(nome.c_str());

	for (i = 0; i < getHeadSize(); i++) {
 		nova_imagem <<  getImagem()[i];
 			}
 	nova_imagem << '\n';
 	for (i = i+1; i < getHeadSize(); i++) {
 		if ((unsigned char) getImagem()[i] < getNivelCor()/2) {
 			nova_imagem << 0;
			} else {
 			nova_imagem << getNivelCor();
		}
 	}

	nova_imagem.close();
}
