
#ifndef FILTROS_HPP
#define FILTROS_HPP
#include <iostream>
#include <string>
#include "imagem.hpp"

using namespace std;
//criaçao da classe filtros herdando atributos publicos de imagemPPM
class Filtros : public ImagemPPM {
public:
	Filtros();
	~Filtros();

	virtual void AplicaFiltro() = 0;

};

#endif
