#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP
#include <iostream>
#include <string>
#include "filtros.hpp"

using namespace std;

class Polarizado : public Filtros {
public:
	Polarizado();
	~Polarizado();

	void AplicaFiltro();

};
#endif
