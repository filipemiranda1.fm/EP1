#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP
#include <iostream>
#include <string>
#include "filtros.hpp"

using namespace std;

class Negativo : public Filtros {
public:
	Negativo();
	~Negativo();

	void AplicaFiltro();

};
#endif
