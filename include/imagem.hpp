#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>
#include <iostream>

using namespace std;

// Criando a classe imagem

class ImagemPPM {
        private:
        string imagem;
	string numero_magico;
	unsigned char nivel_cor;
        int tamanho_total;
        int tamanho_imagem;
        int largura;
        int altura;
	int head_size;
        public:
        ImagemPPM(); // Construtor padrão
        ~ImagemPPM(); // Destrutor

// Métodos acessores

        string getImagem();
        void setImagem(string imagem);
        string getNumeroMagico();
        void setNumeroMagico(string numero_magico);
        unsigned char getNivelCor();
        void setNivelCor(unsigned char nivel_cor);
        int getTamanhoTotal();
        void setTamanhoTotal(int tamanho_total);
        int getTamanhoImagem();
        void setTamanhoImagem(int tamanho_imagem);
        int getLargura();
        void setLargura(int largura);
        int getAltura();
        void setAltura(int Altura);
	int getHeadSize();
	void setHeadSize(int head_size);

// Funções de abrir a imagem e extrair os dados do cabeçalho

        void AbrirImagem();
        void ExtrairAtributos();
	void ExtrairHead();
};

#endif


