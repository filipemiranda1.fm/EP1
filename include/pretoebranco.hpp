#ifndef PRETOEBRANCO_HPP
#define PRETOEBRANCO_HPP
#include <iostream>
#include <string>
#include "filtros.hpp"

using namespace std;

class Pretoebranco : public Filtros {
public:
        Pretoebranco();
        ~Pretoebranco();

        void AplicaFiltro();

};
#endif


