### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Salve a imagem .ppm a ser lida na pasta raiz do aplicativo;
* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**
* Digite a opção desejada para o filtro que queira aplicar;
* Insira o nome da imagem a ser aberta, padrao de teste xmas.ppm;
* Insira o novo nome a ser salvo na mesma raiz do aplicativo.
